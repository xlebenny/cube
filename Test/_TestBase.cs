using Microsoft.Extensions.DependencyInjection;
using Models;

public class TestBase : IDisposable
{
    protected readonly Database Database;
    protected readonly CubeService CubeService;
    //
    public TestBase()
    {
        var services = new ServiceCollection();
        var option = new CommandLineOption();
        InjectionHelper.ConfigureServices(services, option);
        //
        var serviceProvider = services.BuildServiceProvider();
        Database = serviceProvider.GetService<Database>()!;
        CubeService = serviceProvider.GetService<CubeService>()!;
    }

    public void Dispose()
    {
        // do nothing
    }
}
