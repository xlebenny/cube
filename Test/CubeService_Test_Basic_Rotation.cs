using Entities;
using Models;

public partial class CubeService_Test : TestBase
{
    [Fact]
    public void Basic_Rotation_FrontClockwise()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "F");
        var faceName = Face.NameEnum.C;
        //
        CheckRotation(cube, command, faceName);
    }

    [Fact]
    public void Basic_Rotation_RightClockwise()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "R");
        var faceName = Face.NameEnum.D;
        //
        CheckRotation(cube, command, faceName);
    }

    [Fact]
    public void Basic_Rotation_UpClockwise()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "U");
        var faceName = Face.NameEnum.A;
        //
        CheckRotation(cube, command, faceName);
    }

    [Fact]
    public void Basic_Rotation_BottomClockwise()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "B");
        var faceName = Face.NameEnum.E;
        //
        CheckRotation(cube, command, faceName);
    }

    [Fact]
    public void Basic_Rotation_LeftClockwise()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "L");
        var faceName = Face.NameEnum.B;
        //
        CheckRotation(cube, command, faceName);
    }

    [Fact]
    public void Basic_Rotation_DownClockwise()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "D");
        var faceName = Face.NameEnum.F;
        //
        CheckRotation(cube, command, faceName);
    }

    [Fact]
    public void Basic_Rotation_FrontInverted()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "F'");
        var faceName = Face.NameEnum.C;
        //
        CheckRotation(cube, command, faceName);
    }

    [Fact]
    public void Basic_Rotation_RightInverted()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "R'");
        var faceName = Face.NameEnum.D;
        CheckRotation(cube, command, faceName);
    }

    [Fact]
    public void Basic_Rotation_UpInverted()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "U'");
        var faceName = Face.NameEnum.A;
        //
        CheckRotation(cube, command, faceName);
    }

    [Fact]
    public void Basic_Rotation_BottomInverted()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "B'");
        var faceName = Face.NameEnum.E;
        //
        CheckRotation(cube, command, faceName);
    }

    [Fact]
    public void Basic_Rotation_LeftInverted()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "L'");
        var faceName = Face.NameEnum.B;
        //
        CheckRotation(cube, command, faceName);
    }

    [Fact]
    public void Basic_Rotation_DownInverted()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "D'");
        var faceName = Face.NameEnum.F;
        //
        CheckRotation(cube, command, faceName);
    }

    private void CheckRotation(Cube cube, ScrambleCommand command, Face.NameEnum faceName)
    {
        var face = cube.Faces
                    .Single(x => x.Name == faceName);
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Top && x.ColumnName == Block.ColumnNameEnum.Left)
            .Color = Block.ColorEnum.Blue;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Top && x.ColumnName == Block.ColumnNameEnum.Middle)
            .Color = Block.ColorEnum.Yellow;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Top && x.ColumnName == Block.ColumnNameEnum.Right)
            .Color = Block.ColorEnum.Orange;

        //
        // command * 0
        //
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = faceName, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = faceName, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Yellow },
                new BlockAndColor() { FaceName = faceName, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Orange },
            }
        );

        //
        // command * 1
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = faceName, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = faceName, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Yellow },
                new BlockAndColor() { FaceName = faceName, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Orange },
            }
        );

        //
        // command * 2
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = faceName, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Orange },
                new BlockAndColor() { FaceName = faceName, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Yellow },
                new BlockAndColor() { FaceName = faceName, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 3
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = faceName, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Orange },
                new BlockAndColor() { FaceName = faceName, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Yellow },
                new BlockAndColor() { FaceName = faceName, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 4
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = faceName, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = faceName, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Yellow },
                new BlockAndColor() { FaceName = faceName, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Orange },
            }
        );
    }
}