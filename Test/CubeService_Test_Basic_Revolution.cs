using Entities;
using Models;

public partial class CubeService_Test : TestBase
{
    [Fact]
    public void Basic_Revolution_FrontClockwise()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "F");
        //
        var face = cube.Faces
            .Single(x => x.Name == Face.NameEnum.A);
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Bottom && x.ColumnName == Block.ColumnNameEnum.Left)
            .Color = Block.ColorEnum.Red;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Bottom && x.ColumnName == Block.ColumnNameEnum.Middle)
            .Color = Block.ColorEnum.Green;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Bottom && x.ColumnName == Block.ColumnNameEnum.Right)
            .Color = Block.ColorEnum.Blue;

        //
        // command * 0
        //
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 1
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 2
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Red },
            }
        );

        //
        // command * 3
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Red },
            }
        );

        //
        // command * 4
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );
    }

    [Fact]
    public void Basic_Revolution_RightClockwise()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "R");
        //
        var face = cube.Faces
            .Single(x => x.Name == Face.NameEnum.A);
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Top && x.ColumnName == Block.ColumnNameEnum.Right)
            .Color = Block.ColorEnum.Red;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Middle && x.ColumnName == Block.ColumnNameEnum.Right)
            .Color = Block.ColorEnum.Green;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Bottom && x.ColumnName == Block.ColumnNameEnum.Right)
            .Color = Block.ColorEnum.Blue;

        //
        // command * 0
        //
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 1
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
            }
        );

        //
        // command * 2
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 3
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 4
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );
    }

    [Fact]
    public void Basic_Revolution_UpClockwise()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "U");
        //
        var face = cube.Faces
            .Single(x => x.Name == Face.NameEnum.B);
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Top && x.ColumnName == Block.ColumnNameEnum.Left)
            .Color = Block.ColorEnum.Red;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Top && x.ColumnName == Block.ColumnNameEnum.Middle)
            .Color = Block.ColorEnum.Green;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Top && x.ColumnName == Block.ColumnNameEnum.Right)
            .Color = Block.ColorEnum.Blue;

        //
        // command * 0
        //
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 1
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 2
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 3
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 4
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );
    }

    [Fact]
    public void Basic_Revolution_BottomClockwise()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "B");
        //
        var face = cube.Faces
            .Single(x => x.Name == Face.NameEnum.A);
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Top && x.ColumnName == Block.ColumnNameEnum.Left)
            .Color = Block.ColorEnum.Red;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Top && x.ColumnName == Block.ColumnNameEnum.Middle)
            .Color = Block.ColorEnum.Green;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Top && x.ColumnName == Block.ColumnNameEnum.Right)
            .Color = Block.ColorEnum.Blue;

        //
        // command * 0
        //
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 1
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
            }
        );

        //
        // command * 2
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Red },
            }
        );

        //
        // command * 3
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 4
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );
    }

    [Fact]
    public void Basic_Revolution_LeftClockwise()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "L");
        //
        var face = cube.Faces
            .Single(x => x.Name == Face.NameEnum.A);
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Top && x.ColumnName == Block.ColumnNameEnum.Left)
            .Color = Block.ColorEnum.Red;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Middle && x.ColumnName == Block.ColumnNameEnum.Left)
            .Color = Block.ColorEnum.Green;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Bottom && x.ColumnName == Block.ColumnNameEnum.Left)
            .Color = Block.ColorEnum.Blue;

        //
        // command * 0
        //
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 1
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 2
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 3
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Red },
            }
        );

        //
        // command * 4
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
            }
        );
    }

    [Fact]
    public void Basic_Revolution_DownClockwise()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "D");
        //
        var face = cube.Faces
            .Single(x => x.Name == Face.NameEnum.B);
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Bottom && x.ColumnName == Block.ColumnNameEnum.Left)
            .Color = Block.ColorEnum.Red;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Bottom && x.ColumnName == Block.ColumnNameEnum.Middle)
            .Color = Block.ColorEnum.Green;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Bottom && x.ColumnName == Block.ColumnNameEnum.Right)
            .Color = Block.ColorEnum.Blue;

        //
        // command * 0
        //
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 1
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 2
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 3
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 4
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );
    }

    [Fact]
    public void Basic_Revolution_FrontInverted()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "F'");
        //
        var face = cube.Faces
            .Single(x => x.Name == Face.NameEnum.A);
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Bottom && x.ColumnName == Block.ColumnNameEnum.Left)
            .Color = Block.ColorEnum.Red;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Bottom && x.ColumnName == Block.ColumnNameEnum.Middle)
            .Color = Block.ColorEnum.Green;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Bottom && x.ColumnName == Block.ColumnNameEnum.Right)
            .Color = Block.ColorEnum.Blue;

        //
        // command * 0
        //
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 1
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Red },
            }
        );

        //
        // command * 2
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Red },
            }
        );

        //
        // command * 3
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 4
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );
    }

    [Fact]
    public void Basic_Revolution_RightInverted()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "R'");
        //
        var face = cube.Faces
            .Single(x => x.Name == Face.NameEnum.A);
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Top && x.ColumnName == Block.ColumnNameEnum.Right)
            .Color = Block.ColorEnum.Red;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Middle && x.ColumnName == Block.ColumnNameEnum.Right)
            .Color = Block.ColorEnum.Green;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Bottom && x.ColumnName == Block.ColumnNameEnum.Right)
            .Color = Block.ColorEnum.Blue;

        //
        // command * 0
        //
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 1
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()

            {
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 2
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 3
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
            }
        );

        //
        // command * 4
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );
    }

    [Fact]
    public void Basic_Revolution_UpInverted()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "U'");
        //
        var face = cube.Faces
            .Single(x => x.Name == Face.NameEnum.B);
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Top && x.ColumnName == Block.ColumnNameEnum.Left)
            .Color = Block.ColorEnum.Red;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Top && x.ColumnName == Block.ColumnNameEnum.Middle)
            .Color = Block.ColorEnum.Green;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Top && x.ColumnName == Block.ColumnNameEnum.Right)
            .Color = Block.ColorEnum.Blue;

        //
        // command * 0
        //
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 1
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 2
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 3
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 4
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );
    }

    [Fact]
    public void Basic_Revolution_BottomInverted()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "B'");
        //
        var face = cube.Faces
            .Single(x => x.Name == Face.NameEnum.A);
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Top && x.ColumnName == Block.ColumnNameEnum.Left)
            .Color = Block.ColorEnum.Red;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Top && x.ColumnName == Block.ColumnNameEnum.Middle)
            .Color = Block.ColorEnum.Green;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Top && x.ColumnName == Block.ColumnNameEnum.Right)
            .Color = Block.ColorEnum.Blue;

        //
        // command * 0
        //
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 1
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()

            {
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 2
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Red },
            }
        );

        //
        // command * 3
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
            }
        );

        //
        // command * 4
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );
    }

    [Fact]
    public void Basic_Revolution_LeftInverted()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "L'");
        //
        var face = cube.Faces
            .Single(x => x.Name == Face.NameEnum.A);
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Top && x.ColumnName == Block.ColumnNameEnum.Left)
            .Color = Block.ColorEnum.Red;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Middle && x.ColumnName == Block.ColumnNameEnum.Left)
            .Color = Block.ColorEnum.Green;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Bottom && x.ColumnName == Block.ColumnNameEnum.Left)
            .Color = Block.ColorEnum.Blue;

        //
        // command * 0
        //
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 1
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Red },
            }
        );

        //
        // command * 2
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 3
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 4
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
            }
        );
    }

    [Fact]
    public void Basic_Revolution_DownInverted()
    {
        var cube = InitialCube();
        var command = Database.Scrambles
            .Single(x => x.Name == "D'");
        //
        var face = cube.Faces
            .Single(x => x.Name == Face.NameEnum.B);
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Bottom && x.ColumnName == Block.ColumnNameEnum.Left)
            .Color = Block.ColorEnum.Red;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Bottom && x.ColumnName == Block.ColumnNameEnum.Middle)
            .Color = Block.ColorEnum.Green;
        face.Blocks
            .Single(x => x.RowName == Block.RowNameEnum.Bottom && x.ColumnName == Block.ColumnNameEnum.Right)
            .Color = Block.ColorEnum.Blue;

        //
        // command * 0
        //
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 1
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 2
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 3
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );

        //
        // command * 4
        //
        CubeService.Scramble(command, cube);
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
            }
        );
    }
}