using Entities;
using Models;

public partial class CubeService_Test : TestBase
{
    [Fact]
    public void Basic_NoCommand()
    {
        var cube = CubeService.InitialCube();
        //
        CheckBlock(
            cube,
            new List<BlockAndColor>()
            {
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.White },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.White },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.White },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.White },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.White },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.White },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.White },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.White },
                new BlockAndColor() { FaceName = Face.NameEnum.A, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.White },
                //
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Orange },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Orange },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Orange },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Orange },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Orange },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Orange },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Orange },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Orange },
                new BlockAndColor() { FaceName = Face.NameEnum.B, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Orange },
                //
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Green },
                new BlockAndColor() { FaceName = Face.NameEnum.C, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Green },
                //
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Red },
                new BlockAndColor() { FaceName = Face.NameEnum.D, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Red },
                //
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Blue },
                new BlockAndColor() { FaceName = Face.NameEnum.E, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Blue },
                //
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Yellow },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Yellow },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Top, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Yellow },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Yellow },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Yellow },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Middle, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Yellow },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Left, Color = Block.ColorEnum.Yellow },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Middle, Color = Block.ColorEnum.Yellow },
                new BlockAndColor() { FaceName = Face.NameEnum.F, RowName = Block.RowNameEnum.Bottom, ColumnName = Block.ColumnNameEnum.Right, Color = Block.ColorEnum.Yellow },
            }
        );
    }
}