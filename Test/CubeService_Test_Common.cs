using Entities;
using Models;

public partial class CubeService_Test : TestBase
{
    private Block.ColorEnum DisableColor = Block.ColorEnum.White;

    private void CheckBlock(Cube cube, List<BlockAndColor> blockAndColors)
    {
        var flatted = cube.Faces
            .SelectMany(face =>
                face.Blocks.Select(block => new
                {
                    FaceName = face.Name,
                    RowName = block.RowName,
                    ColumnName = block.ColumnName,
                    Color = block.Color,
                })
            )
            .ToList();
        //
        var changedItems = flatted
            .Where(x =>
                blockAndColors.Any(y =>
                    y.FaceName == x.FaceName
                    && y.RowName == x.RowName
                    && y.ColumnName == x.ColumnName
                )
            )
            .ToList();
        var disableds = flatted
            .Where(x =>
                false == blockAndColors.Any(y =>
                    y.FaceName == x.FaceName
                    && y.RowName == x.RowName
                    && y.ColumnName == x.ColumnName
                )
            )
            .ToList();
        //
        foreach (var actual in changedItems)
        {
            var except = blockAndColors
                .Single(x =>
                    x.FaceName == actual.FaceName
                    && x.RowName == actual.RowName
                    && x.ColumnName == actual.ColumnName
                );
            Assert.True(
                except.Color == actual.Color,
                $"Except: {except.Color}, Actual: {actual.Color}, Position: ({actual.FaceName}, {actual.RowName}, {actual.ColumnName})"
            );
        }
        foreach (var actual in disableds)
        {
            Assert.True(
                DisableColor == actual.Color,
                $"Except: {DisableColor}, Actual: {actual.Color}, Position: ({actual.FaceName}, {actual.RowName}, {actual.ColumnName})"
            );
        }
    }

    private Cube InitialCube()
    {
        var parameter = new InitialCubeParameter()
        {
            RowNames = new List<Block.RowNameEnum>()
            {
                Block.RowNameEnum.Top,
                Block.RowNameEnum.Middle,
                Block.RowNameEnum.Bottom,
            },
            ColumnNames = new List<Block.ColumnNameEnum>()
            {
                Block.ColumnNameEnum.Left,
                Block.ColumnNameEnum.Middle,
                Block.ColumnNameEnum.Right,
            },
            FaceAndColorSettings = new List<InitialCubeParameter.FaceAndColorSetting>()
            {
                new InitialCubeParameter.FaceAndColorSetting() { FaceName = Face.NameEnum.A, Color = DisableColor },
                new InitialCubeParameter.FaceAndColorSetting() { FaceName = Face.NameEnum.B, Color = DisableColor },
                new InitialCubeParameter.FaceAndColorSetting() { FaceName = Face.NameEnum.C, Color = DisableColor },
                new InitialCubeParameter.FaceAndColorSetting() { FaceName = Face.NameEnum.D, Color = DisableColor },
                new InitialCubeParameter.FaceAndColorSetting() { FaceName = Face.NameEnum.E, Color = DisableColor },
                new InitialCubeParameter.FaceAndColorSetting() { FaceName = Face.NameEnum.F, Color = DisableColor },
            },
        };
        //
        var result = CubeService.InitialCube(parameter);
        return result;
    }

    private class BlockAndColor
    {
        public Face.NameEnum FaceName { get; set; }
        public Block.RowNameEnum RowName { get; set; }
        public Block.ColumnNameEnum ColumnName { get; set; }
        public Block.ColorEnum Color { get; set; }
    }
}