namespace Entities
{
    public class Cube
    {
        public List<Face> Faces { get; set; } = new List<Face>();
        public List<Block.RowNameEnum> RowNames { get; set; } = new List<Block.RowNameEnum>();
        public List<Block.ColumnNameEnum> ColumnNames { get; set; } = new List<Block.ColumnNameEnum>();
    }

    public class Face
    {
        public NameEnum? Name { get; set; }
        public List<Block> Blocks { get; set; } = new List<Block>();
        //
        public enum NameEnum
        {
            A,
            B,
            C,
            D,
            E,
            F,
        }
    }

    public class Block
    {
        public RowNameEnum RowName { get; set; }
        public ColumnNameEnum ColumnName { get; set; }
        public ColorEnum? Color { get; set; }
        //
        public enum RowNameEnum
        {
            Top,
            Middle,
            Bottom,
        }
        //
        public enum ColumnNameEnum
        {
            Left,
            Middle,
            Right,
        }
        //
        public enum ColorEnum
        {
            Orange = 'O',
            Green = 'G',
            Red = 'R',
            Blue = 'B',
            White = 'W',
            Yellow = 'Y',
        }
    }

    public class ScrambleCommand
    {
        public string Name { get; set; } = "[Name]";
        public List<ScrambleGroup> ScrambleGroups { get; set; } = new List<ScrambleGroup>();
    }

    public class ScrambleGroup
    {
        public List<ScrambleStep> ScrambleSteps { get; set; } = new List<ScrambleStep>();
    }

    public class ScrambleStep
    {
        public int Order { get; set; }
        public Face.NameEnum FaceName { get; set; }
        public Block.RowNameEnum RowName { get; set; }
        public Block.ColumnNameEnum ColumnName { get; set; }
    }
}