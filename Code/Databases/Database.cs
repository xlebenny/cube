public class Database
{
    public List<Entities.ScrambleCommand> Scrambles = new List<Entities.ScrambleCommand>()
    {
        new Entities.ScrambleCommand() {
            Name = "F",
            ScrambleGroups = new List<Entities.ScrambleGroup>() {
                // rotation
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                // revolution
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                    },
                },
            },
        },
        new Entities.ScrambleCommand() {
            Name = "R",
            ScrambleGroups = new List<Entities.ScrambleGroup>() {
                // rotation
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                // revolution
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
            },
        },
        new Entities.ScrambleCommand() {
            Name = "U",
            ScrambleGroups = new List<Entities.ScrambleGroup>() {
                // rotation
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                // revolution
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                    },
                },
            },
        },
        new Entities.ScrambleCommand() {
            Name = "B",
            ScrambleGroups = new List<Entities.ScrambleGroup>() {
                // rotation
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                // revolution
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
            },
        },
        new Entities.ScrambleCommand() {
            Name = "L",
            ScrambleGroups = new List<Entities.ScrambleGroup>() {
                // rotation
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                // revolution
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                    },
                },
            },
        },
        new Entities.ScrambleCommand() {
            Name = "D",
            ScrambleGroups = new List<Entities.ScrambleGroup>() {
                // rotation
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                // revolution
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
            },
        },
        //
        //
        //
        new Entities.ScrambleCommand() {
            Name = "F'",
            ScrambleGroups = new List<Entities.ScrambleGroup>() {
                // rotation
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                // revolution
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                    },
                },
            },
        },
          new Entities.ScrambleCommand() {
            Name = "R'",
            ScrambleGroups = new List<Entities.ScrambleGroup>() {
                // rotation
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                // revolution
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
            },
        },
        new Entities.ScrambleCommand() {
            Name = "U'",
            ScrambleGroups = new List<Entities.ScrambleGroup>() {
                // rotation
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                // revolution
                 new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                    },
                },
            },
        },
        new Entities.ScrambleCommand() {
            Name = "B'",
            ScrambleGroups = new List<Entities.ScrambleGroup>() {
                // rotation
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                // revolution
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
            },
        },
        new Entities.ScrambleCommand() {
            Name = "L'",
            ScrambleGroups = new List<Entities.ScrambleGroup>() {
                // rotation
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                // revolution
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.A, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                    },
                },
            },
        },
        new Entities.ScrambleCommand() {
            Name = "D'",
            ScrambleGroups = new List<Entities.ScrambleGroup>() {
                // rotation
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Top, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Middle, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.F, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                // revolution
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Left, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Middle, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
                new Entities.ScrambleGroup() {
                    ScrambleSteps = new List<Entities.ScrambleStep>() {
                        new Entities.ScrambleStep() { Order = 4, FaceName = Entities.Face.NameEnum.B, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 3, FaceName = Entities.Face.NameEnum.C, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 2, FaceName = Entities.Face.NameEnum.D, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                        new Entities.ScrambleStep() { Order = 1, FaceName = Entities.Face.NameEnum.E, ColumnName = Entities.Block.ColumnNameEnum.Right, RowName = Entities.Block.RowNameEnum.Bottom, },
                    },
                },
            },
        },
        //
    };
}