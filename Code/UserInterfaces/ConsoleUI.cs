using Entities;
using Models;

public class ConsoleUI
{
    private readonly ILogger Logger;
    private readonly GlobalContext GlobalContext;
    private readonly CubeService CubeService;
    //
    public ConsoleUI(
        ILoggerFactory loggerFactory, GlobalContext globalContext,
        CubeService cubeService
    )
    {
        Logger = loggerFactory.CreateLogger<ConsoleUI>();
        GlobalContext = globalContext;
        CubeService = cubeService;
    }

    public void Play()
    {
        var cube = CubeService.InitialCube();
        var commands = CubeService.ListScrambleCommands();
        var commandByName = CubeService
            .ListScrambleCommands()
            .ToDictionary(x => x.Name);
        //
        Console.Clear();
        while (true)
        {
            Console.WriteLine("Cube:");
            DisplayCube("  ", cube);
            Console.WriteLine("");
            //
            Console.WriteLine("Available commands: ");
            DisplayCommand("  ", commands);
            Console.WriteLine("");
            //
            //
            var isExit = false;
            ScrambleCommand? command = null;
            while (isExit == false && command == null)
            {
                Console.Write("Please input a command (-1 is exit): ");
                var input = Console.ReadLine()!;
                Logger.LogDebug($"input: {input}");
                //
                isExit = (input == "-1");
                Logger.LogDebug($"isExit: {isExit}");
                //
                commandByName.TryGetValue(input, out command);
                Logger.LogDebug($"command: {command?.Name ?? "[Null]"}");
            }
            if (isExit) { Console.WriteLine("Bye."); break; }
            else if (command == null) { Console.WriteLine("Invalid command."); continue; }
            //
            CubeService.Scramble(command, cube);
            Console.Clear();
        }
    }

    private void DisplayCube(string tab, Cube cube)
    {
        var colorMapping = new Dictionary<Block.ColorEnum, string>()
        {
            { Block.ColorEnum.Blue, "B" },
            { Block.ColorEnum.Green, "G" },
            { Block.ColorEnum.Orange, "O" },
            { Block.ColorEnum.Red, "R" },
            { Block.ColorEnum.White, "W" },
            { Block.ColorEnum.Yellow, "Y" },
        };
        //
        // To Reviewer:
        //   In version 1, I placed calculated view in service,
        //   But it will total difference in Console / Web, 
        //   So this logic should be per UI
        var cubeRows = new List<List<Face.NameEnum?>>()
        {
            new List<Face.NameEnum?>() { null,            Face.NameEnum.A, null,            null },
            new List<Face.NameEnum?>() { Face.NameEnum.B, Face.NameEnum.C, Face.NameEnum.D, Face.NameEnum.E },
            new List<Face.NameEnum?>() { null,            Face.NameEnum.F, null,            null },
        };
        var dummyFace = CubeService.InitialFace(cube.RowNames, cube.ColumnNames, null, null);
        var allFaces = new List<Face>() { dummyFace }
            .Concat(cube.Faces)
            .ToList();
        //
        // To Reviewer:
        //   This function only owned by this method,
        //   either extract whole method (`DisplayCube`) to class `FooFormatter`,
        //   or just make `buildLinesFunc` to variable
        var buildLinesFunc = (Func<Face, Block, string> itemFormatterFunc) =>
        {
            var lines = new List<string>();
            foreach (var cubeRow in cubeRows)
            {
                var horizontalLineItems = new List<string>();
                foreach (var rowName in cube.RowNames)
                {
                    var items = new List<string>();
                    horizontalLineItems.Clear();
                    //
                    items.Add(" | ");
                    horizontalLineItems.Add(" +-");
                    foreach (var cubeColumn in cubeRow)
                    {
                        var face = allFaces
                            .Where(x => x.Name == cubeColumn)
                            .Single();

                        var padding = " ";
                        var horizontalLinePadding = "-";
                        foreach (var columnName in cube.ColumnNames)
                        {
                            var block = face
                                .Blocks
                                .Where(x => x.RowName == rowName && x.ColumnName == columnName)
                                .Single();
                            var itemString = itemFormatterFunc(face, block);
                            //
                            items.Add(itemString);
                            horizontalLineItems.Add(
                                string.Join("", Enumerable.Range(1, itemString.Length).Select(_ => "-"))
                            );
                            //
                            items.Add(padding);
                            horizontalLineItems.Add(horizontalLinePadding);
                        }
                        //
                        var hasPadding = items.Count > 0 && items.Last() == padding;
                        if (hasPadding)
                        {
                            items = items.Take(items.Count - 1).ToList();
                            horizontalLineItems = horizontalLineItems.Take(horizontalLineItems.Count - 1).ToList();
                        }
                        //
                        items.Add(" | ");
                        horizontalLineItems.Add("-+ ");
                    }
                    //
                    var line = $"{tab}{string.Join("", items)}";
                    lines.Add(line);
                }
                var horizontalLine = $"{tab}{string.Join("", horizontalLineItems)}";
                lines.Add(string.Join("", horizontalLine));
            }
            if (lines.Count > 0) lines = new List<string>() { lines.Last() }.Concat(lines).ToList();
            //
            //
            var result = string.Join(Environment.NewLine, lines);
            return result;
        };
        //
        var str = buildLinesFunc((Face face, Block block) =>
        {
            var colorStr = (block.Color.HasValue)
                ? colorMapping[block.Color.Value]
                : "x";
            return $"{colorStr}";
        });
        Console.WriteLine(str);
        Console.WriteLine();
        //
        var rowMapping = new Dictionary<Block.RowNameEnum, string>()
        {
            { Block.RowNameEnum.Top, "T" },
            { Block.RowNameEnum.Middle, "M" },
            { Block.RowNameEnum.Bottom, "B" },
        };
        var columnMapping = new Dictionary<Block.ColumnNameEnum, string>()
        {
            { Block.ColumnNameEnum.Left, "L"},
            { Block.ColumnNameEnum.Middle, "M"},
            { Block.ColumnNameEnum.Right, "R"},
        };
        var debugStr = buildLinesFunc((Face face, Block block) =>
        {
            var colorStr = (block.Color.HasValue)
                ? colorMapping[block.Color.Value]
                : " ";
            var faceStr = (face.Name.HasValue)
                ? face.Name.ToString()
                : " ";
            return $"({colorStr}, {faceStr}, {rowMapping[block.RowName]}, {columnMapping[block.ColumnName]})";
        });
        Logger.LogDebug(debugStr);
    }

    private void DisplayCommand(string tab, List<ScrambleCommand> commands)
    {
        var itemPerLine = 6;
        for (var i = 0; i < commands.Count; i++)
        {
            var command = commands[i];
            //
            var line = $"{tab}{String.Join(' ', command.Name)}";
            Console.Write(line);
            //
            var isNewLine = (((i + 1) % itemPerLine) == 0);
            if (isNewLine) Console.WriteLine();
        }
    }
}
