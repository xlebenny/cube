using Microsoft.Extensions.DependencyInjection;
using Models;

public class InjectionHelper
{
    public static void ConfigureServices(ServiceCollection services, CommandLineOption option)
    {
        var globalContext = new GlobalContext()
        {
            Mode = option.Mode,
        };
        services.AddSingleton(globalContext);
        //
        var database = new Database();
        services.AddSingleton(database);
        //
        var loggerFactory = LoggerFactory.Create(builder =>
        {
            builder.ClearProviders().AddConsole();
            if (globalContext.Mode == GlobalContext.ModeEnum.Debug) builder.AddFilter(level => level >= LogLevel.Debug);
        });
        services.AddSingleton(loggerFactory);
        //
        services.AddScoped<CubeService>();
        services.AddScoped<ConsoleUI>();
    }
}