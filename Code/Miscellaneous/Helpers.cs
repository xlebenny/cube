using System.Text.Json;

public static class Helper
{
    public static string ToJson(object obj)
    {
        var json = JsonSerializer.Serialize(obj);
        return json;
    }
}

public static class HelperExtension
{
    public static string ToJson(this object obj)
    {
        return Helper.ToJson(obj);
    }
}
