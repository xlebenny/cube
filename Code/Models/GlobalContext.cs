namespace Models
{
    public class GlobalContext
    {
        public ModeEnum Mode { get; set; }

        public enum ModeEnum
        {
            Production,
            Debug,
            UnitTest,
        }
    }
}