using Entities;

namespace Models
{
    public class InitialCubeParameter
    {
        public List<Block.RowNameEnum> RowNames = new List<Block.RowNameEnum>();
        public List<Block.ColumnNameEnum> ColumnNames = new List<Block.ColumnNameEnum>();
        public List<FaceAndColorSetting> FaceAndColorSettings = new List<FaceAndColorSetting>();
        //
        public class FaceAndColorSetting
        {
            public Face.NameEnum FaceName { get; set; }
            public Block.ColorEnum Color { get; set; }
        }
    }
}