using CommandLine;

namespace Models
{
    public class CommandLineOption
    {
        [Option(
            'm', "mode",
            HelpText = "Production, Debug",
            Required = false,
            Default = GlobalContext.ModeEnum.Production
        )]
        public GlobalContext.ModeEnum Mode { get; set; }
    }
}