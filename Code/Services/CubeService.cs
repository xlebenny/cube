using Entities;
using Models;

public class CubeService
{
    private readonly ILogger Logger;
    private readonly GlobalContext GlobalContext;
    private readonly Database Database;
    //
    public CubeService(
        ILoggerFactory loggerFactory, GlobalContext globalContext,
        Database database
    )
    {
        Logger = loggerFactory.CreateLogger<ConsoleUI>();
        GlobalContext = globalContext;
        Database = database;
    }

    public void Scramble(Entities.ScrambleCommand scrambleCommand, Entities.Cube cube)
    {
        // To Reviewer:
        //   You maybe think something like formula (clockwise / anti-clockwise),
        //   But in real world,
        //     user may logical at Day 1,
        //     but after day by day, they will found many EXCEPTION CASE
        //     because they management is conside CASE BY CASE
        //   So, I only assume blocks will "shift" like 1 -> 2 -> 3 -> 4 -> 1,
        //     shift from / to which location is CASE BY CASE (Maybe not clockwise / anti-clockwise later)
        //
        foreach (var group in scrambleCommand.ScrambleGroups)
        {
            var steps = group.ScrambleSteps
                .OrderBy(x => x.Order)
                .ToList();
            //
            var blocks = steps
                .Select(step =>
                    cube
                        .Faces.Single(face => face.Name == step.FaceName)
                        .Blocks.Single(block => block.ColumnName == step.ColumnName && block.RowName == step.RowName)
                )
                .ToList();
            var assignments = blocks
                .Select((block, i) => new
                {
                    Block = block,
                    NewColor = blocks[((i - 1) >= 0 ? (i - 1) : (blocks.Count - 1))].Color,
                })
                .ToList();
            //
            foreach (var assignment in assignments)
            {
                assignment.Block.Color = assignment.NewColor;
            }
        }
    }

    public List<ScrambleCommand> ListScrambleCommands()
    {
        // To Reviewer:
        //    assume this is typical MVC application,
        //    will convert entity --> model in controller layer, using "AutoMapper"
        return Database.Scrambles;
    }

    // To Reviewer:
    //   Maybe "Create" is more meaningful,
    //   But some developer will ask "it's create and saved in database?"
    //   So name to "Initial" will better
    public Cube InitialCube()
    {
        // To Reviewer:
        //  - assume "user" just have this use case,
        //      but we guess this possibility change later
        var parameter = new InitialCubeParameter()
        {
            RowNames = new List<Block.RowNameEnum>()
            {
                Block.RowNameEnum.Top,
                Block.RowNameEnum.Middle,
                Block.RowNameEnum.Bottom,
            },
            ColumnNames = new List<Block.ColumnNameEnum>()
            {
                Block.ColumnNameEnum.Left,
                Block.ColumnNameEnum.Middle,
                Block.ColumnNameEnum.Right,
            },
            FaceAndColorSettings = new List<InitialCubeParameter.FaceAndColorSetting>()
            {
                new InitialCubeParameter.FaceAndColorSetting() { FaceName = Face.NameEnum.A, Color = Block.ColorEnum.White},
                new InitialCubeParameter.FaceAndColorSetting() { FaceName = Face.NameEnum.B, Color = Block.ColorEnum.Orange},
                new InitialCubeParameter.FaceAndColorSetting() { FaceName = Face.NameEnum.C, Color = Block.ColorEnum.Green},
                new InitialCubeParameter.FaceAndColorSetting() { FaceName = Face.NameEnum.D, Color = Block.ColorEnum.Red},
                new InitialCubeParameter.FaceAndColorSetting() { FaceName = Face.NameEnum.E, Color = Block.ColorEnum.Blue},
                new InitialCubeParameter.FaceAndColorSetting() { FaceName = Face.NameEnum.F, Color = Block.ColorEnum.Yellow},
            },
        };
        //
        var result = InitialCube(parameter);
        return result;
    }

    public Cube InitialCube(InitialCubeParameter parameter)
    {
        //
        var result = new Cube()
        {
            Faces = parameter.FaceAndColorSettings
                .Select(x =>
                    InitialFace(parameter.RowNames, parameter.ColumnNames, x.FaceName, x.Color)
                )
                .ToList(),
            RowNames = parameter.RowNames,
            ColumnNames = parameter.ColumnNames,
        };
        //
        return result;
    }

    public Face InitialFace(
        List<Block.RowNameEnum> rowNames, List<Block.ColumnNameEnum> columnNames,
        Face.NameEnum? name, Block.ColorEnum? color
    )
    {
        // ASSUME: Horizontal * Vertical
        var blocks = rowNames
            .SelectMany(x =>
                columnNames.Select(y => new Block() { RowName = x, ColumnName = y, Color = color })
            )
            .ToList();
        //
        var result = new Face()
        {
            Name = name,
            Blocks = blocks,
        };
        return result;
    }
}