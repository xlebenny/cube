﻿global using Microsoft.Extensions.Logging;
//
using CommandLine;
using Microsoft.Extensions.DependencyInjection;
using Models;

public partial class Program
{
    public static void Main(string[] args)
    {
        var praseResult = Parser.Default.ParseArguments<CommandLineOption>(args);

        WithCommandLineOption<CommandLineOption>(
            args,
            option =>
            {
                var services = new ServiceCollection();
                InjectionHelper.ConfigureServices(services, option);
                //
                var serviceProvider = services.BuildServiceProvider();
                serviceProvider.GetService<ConsoleUI>()!.Play();
            });
    }

    private static void WithCommandLineOption<T>(IEnumerable<string> args, Action<T> func)
    {
        var praseResult = Parser.Default.ParseArguments<T>(args);
        praseResult
            .WithParsed(func)
            .WithNotParsed(errors =>
            {
                var helperText = CommandLine.Text.HelpText.AutoBuild(praseResult);
                Console.WriteLine(helperText);
                Environment.Exit(-1);
            });
    }
}